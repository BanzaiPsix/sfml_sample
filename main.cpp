﻿
#include<SFML/Graphics.hpp>
#include<SFML/Window.hpp>
#include<SFML/System.hpp>


using namespace sf;
int main()
{
	srand(time(NULL));

	sf::RenderWindow window(VideoMode(640, 480), "Cat&do(d)ge");
	window.setFramerateLimit(60);
	//фон
	sf::Texture texture;
	if (!texture.loadFromFile("fon.jpg"))
			throw "ERROR: not found Fon" ;
	sf::Sprite back(texture);
		
	
	//коты 
	Texture catTex;
	Sprite cat;

	if (!catTex.loadFromFile("cat.png"))
		throw "Could not load cat.png!";

	cat.setTexture(catTex);
	cat.setScale(Vector2f(0.2f, 0.2f));
	int catSpawnTimer = 15;
	std::vector<Sprite> cats;
	cats.push_back(Sprite(cat));

	//собака  (hp)
	Texture dogeTex;
	Sprite doge;
	int hp = 10;
	RectangleShape hpBar;
	hpBar.setFillColor(Color::Red);
	hpBar.setSize(Vector2f((float)hp * 20.f, 20.f));
	hpBar.setPosition(200.f, 4.f);

	if (!dogeTex.loadFromFile("doge.png"))
		throw "Could not load doge.png!";

	doge.setTexture(dogeTex);
	doge.setScale(Vector2f(0.3f, 0.3f));

	//игра 
	while (window.isOpen() && hp > 0)
	{
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
				window.close();

			if (event.type == Event::KeyPressed && event.key.code == Keyboard::Escape)
				window.close();
		}
		


		//собака(игрок)
		doge.setPosition(doge.getPosition().x, Mouse::getPosition(window).y);

		if (doge.getPosition().y > window.getSize().y - doge.getGlobalBounds().height)
			doge.setPosition(doge.getPosition().x, window.getSize().y - doge.getGlobalBounds().height);

		if (doge.getPosition().y < 0)
			doge.setPosition(doge.getPosition().x, 0);

		//кошачья скорость
		for (size_t i = 0; i < cats.size(); i++)
		{
			cats[i].move(-7.f, 0.f);

			if (cats[i].getPosition().x < 0 - cat.getGlobalBounds().width)
				cats.erase(cats.begin() + i);
		}

		if (catSpawnTimer < 40)
			catSpawnTimer++;

		if (catSpawnTimer >= 40)
		{
			cat.setPosition(window.getSize().x, rand() % int(window.getSize().y - cat.getGlobalBounds().height));
			cats.push_back(Sprite(cat));
			catSpawnTimer = 0;
		}

		//колижен модель 
		for (size_t i = 0; i < cats.size(); i++)
		{
			if (doge.getGlobalBounds().intersects(cats[i].getGlobalBounds()))
			{
				hp--;
				cats.erase(cats.begin() + i);
			}
		}
		//hp
		hpBar.setSize(Vector2f((float)hp * 20.f, 20.f));

		//отрисовка 
		window.clear();
		window.draw(back);//фон
		window.draw(doge);//собака 

		for (size_t i = 0; i < cats.size(); i++)
		{
			window.draw(cats[i]);
		}

		
		window.draw(hpBar);
		
		window.display();

	}
	

	return 0;
}